/********************************************************************************
** Form generated from reading UI file 'weatherre.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WEATHERRE_H
#define UI_WEATHERRE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>
#include <rolllab.h>
#include <softkey.h>

QT_BEGIN_NAMESPACE

class Ui_weatherRE
{
public:
    QWidget *centralWidget;
    QLineEdit *lineEdit;
    QPushButton *pushButton;
    QLabel *day1;
    QLabel *cityname;
    QGroupBox *day1st;
    QLabel *today;
    QLabel *nowtmp;
    QLabel *imgday1;
    QLabel *temperature1;
    QLabel *type1;
    QLabel *feng1;
    QLabel *block1;
    QGroupBox *day2rd;
    QLabel *day2;
    QLabel *imgday2;
    QLabel *temperature2;
    QLabel *type2;
    QLabel *feng2;
    QLabel *block2;
    QGroupBox *groupBox_3;
    QLabel *day4;
    QLabel *imgday4;
    QLabel *temperatrue4;
    QLabel *type4;
    QLabel *feng4;
    QLabel *block4;
    QGroupBox *day3th;
    QLabel *day3;
    QLabel *imgday3;
    QLabel *temperature3;
    QLabel *type3;
    QLabel *feng3;
    QLabel *block3;
    QGroupBox *day5th;
    QLabel *day5;
    QLabel *imgday5;
    QLabel *temperature5;
    QLabel *type5;
    QLabel *feng5;
    QLabel *gaomao;
    QLabel *labbar;
    softKey *softb;
    rollLab *rolllab;
    QPushButton *closeBt;
    QPushButton *hideBt;
    QPushButton *changPicBt;

    void setupUi(QMainWindow *weatherRE)
    {
        if (weatherRE->objectName().isEmpty())
            weatherRE->setObjectName(QStringLiteral("weatherRE"));
        weatherRE->resize(800, 480);
        QIcon icon;
        icon.addFile(QStringLiteral(":/new/prefix1/image/icon.ico"), QSize(), QIcon::Normal, QIcon::Off);
        weatherRE->setWindowIcon(icon);
        weatherRE->setStyleSheet(QStringLiteral(""));
        centralWidget = new QWidget(weatherRE);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setAutoFillBackground(false);
        centralWidget->setStyleSheet(QStringLiteral(""));
        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(0, 0, 180, 30));
        lineEdit->setFocusPolicy(Qt::ClickFocus);
        lineEdit->setAutoFillBackground(false);
        lineEdit->setFrame(true);
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(149, 0, 31, 30));
        pushButton->setFocusPolicy(Qt::NoFocus);
        pushButton->setStyleSheet(QString::fromUtf8("border-image: url(:/new/prefix1/image/\346\237\245\346\211\276.png);"));
        pushButton->setFlat(true);
        day1 = new QLabel(centralWidget);
        day1->setObjectName(QStringLiteral("day1"));
        day1->setGeometry(QRect(0, 30, 270, 100));
        QFont font;
        font.setPointSize(10);
        font.setBold(true);
        font.setWeight(75);
        day1->setFont(font);
        day1->setFocusPolicy(Qt::NoFocus);
        day1->setFrameShape(QFrame::NoFrame);
        day1->setFrameShadow(QFrame::Plain);
        day1->setLineWidth(0);
        day1->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        day1->setMargin(14);
        cityname = new QLabel(centralWidget);
        cityname->setObjectName(QStringLiteral("cityname"));
        cityname->setGeometry(QRect(270, 30, 135, 100));
        QFont font1;
        font1.setPointSize(29);
        cityname->setFont(font1);
        cityname->setFocusPolicy(Qt::NoFocus);
        cityname->setFrameShape(QFrame::NoFrame);
        cityname->setLineWidth(0);
        cityname->setTextFormat(Qt::AutoText);
        cityname->setAlignment(Qt::AlignCenter);
        day1st = new QGroupBox(centralWidget);
        day1st->setObjectName(QStringLiteral("day1st"));
        day1st->setGeometry(QRect(0, 130, 270, 350));
        day1st->setFocusPolicy(Qt::NoFocus);
        day1st->setStyleSheet(QStringLiteral("border:0px;"));
        day1st->setFlat(false);
        day1st->setCheckable(false);
        day1st->setChecked(false);
        today = new QLabel(day1st);
        today->setObjectName(QStringLiteral("today"));
        today->setGeometry(QRect(0, 0, 270, 50));
        QFont font2;
        font2.setPointSize(15);
        font2.setBold(true);
        font2.setWeight(75);
        today->setFont(font2);
        today->setFocusPolicy(Qt::NoFocus);
        today->setLineWidth(0);
        today->setAlignment(Qt::AlignCenter);
        nowtmp = new QLabel(day1st);
        nowtmp->setObjectName(QStringLiteral("nowtmp"));
        nowtmp->setGeometry(QRect(0, 50, 270, 50));
        QFont font3;
        font3.setPointSize(20);
        font3.setBold(true);
        font3.setWeight(75);
        nowtmp->setFont(font3);
        nowtmp->setFocusPolicy(Qt::NoFocus);
        nowtmp->setAlignment(Qt::AlignCenter);
        imgday1 = new QLabel(day1st);
        imgday1->setObjectName(QStringLiteral("imgday1"));
        imgday1->setGeometry(QRect(69, 110, 132, 140));
        imgday1->setFocusPolicy(Qt::NoFocus);
        imgday1->setStyleSheet(QString::fromUtf8("border-image: url(:/new/prefix1/image/\351\233\267\351\230\265\351\233\250.png);"));
        imgday1->setAlignment(Qt::AlignCenter);
        temperature1 = new QLabel(day1st);
        temperature1->setObjectName(QStringLiteral("temperature1"));
        temperature1->setGeometry(QRect(0, 250, 270, 20));
        QFont font4;
        font4.setPointSize(12);
        font4.setBold(true);
        font4.setWeight(75);
        temperature1->setFont(font4);
        temperature1->setFocusPolicy(Qt::NoFocus);
        temperature1->setAlignment(Qt::AlignCenter);
        type1 = new QLabel(day1st);
        type1->setObjectName(QStringLiteral("type1"));
        type1->setGeometry(QRect(0, 285, 270, 25));
        QFont font5;
        font5.setPointSize(13);
        font5.setBold(true);
        font5.setWeight(75);
        type1->setFont(font5);
        type1->setFocusPolicy(Qt::NoFocus);
        type1->setAlignment(Qt::AlignCenter);
        feng1 = new QLabel(day1st);
        feng1->setObjectName(QStringLiteral("feng1"));
        feng1->setGeometry(QRect(0, 311, 270, 30));
        feng1->setFont(font5);
        feng1->setFocusPolicy(Qt::NoFocus);
        feng1->setAlignment(Qt::AlignCenter);
        block1 = new QLabel(day1st);
        block1->setObjectName(QStringLiteral("block1"));
        block1->setGeometry(QRect(267, 15, 20, 350));
        block1->setStyleSheet(QLatin1String("background-color: rgb(237, 249, 255);\n"
"border-radius:175px"));
        block1->setFrameShape(QFrame::NoFrame);
        block1->setFrameShadow(QFrame::Plain);
        block1->setLineWidth(0);
        day2rd = new QGroupBox(centralWidget);
        day2rd->setObjectName(QStringLiteral("day2rd"));
        day2rd->setGeometry(QRect(270, 180, 132, 300));
        day2rd->setFocusPolicy(Qt::NoFocus);
        day2rd->setStyleSheet(QStringLiteral("border:0px;"));
        day2rd->setFlat(false);
        day2rd->setCheckable(false);
        day2rd->setChecked(false);
        day2 = new QLabel(day2rd);
        day2->setObjectName(QStringLiteral("day2"));
        day2->setGeometry(QRect(0, 0, 132, 50));
        QFont font6;
        font6.setPointSize(10);
        day2->setFont(font6);
        day2->setFocusPolicy(Qt::NoFocus);
        day2->setAlignment(Qt::AlignCenter);
        imgday2 = new QLabel(day2rd);
        imgday2->setObjectName(QStringLiteral("imgday2"));
        imgday2->setGeometry(QRect(0, 60, 132, 141));
        imgday2->setFocusPolicy(Qt::NoFocus);
        imgday2->setStyleSheet(QString::fromUtf8("border-image: url(:/new/prefix1/image/\346\231\264.png);"));
        temperature2 = new QLabel(day2rd);
        temperature2->setObjectName(QStringLiteral("temperature2"));
        temperature2->setGeometry(QRect(0, 200, 132, 20));
        temperature2->setFont(font4);
        temperature2->setFocusPolicy(Qt::NoFocus);
        temperature2->setAlignment(Qt::AlignCenter);
        type2 = new QLabel(day2rd);
        type2->setObjectName(QStringLiteral("type2"));
        type2->setGeometry(QRect(0, 230, 132, 25));
        type2->setFont(font);
        type2->setFocusPolicy(Qt::NoFocus);
        type2->setAlignment(Qt::AlignCenter);
        feng2 = new QLabel(day2rd);
        feng2->setObjectName(QStringLiteral("feng2"));
        feng2->setGeometry(QRect(0, 253, 132, 30));
        feng2->setFont(font);
        feng2->setFocusPolicy(Qt::NoFocus);
        feng2->setAlignment(Qt::AlignCenter);
        block2 = new QLabel(day2rd);
        block2->setObjectName(QStringLiteral("block2"));
        block2->setGeometry(QRect(130, 0, 3, 301));
        block2->setStyleSheet(QLatin1String("background-color: rgb(237, 249, 255);\n"
"border-radius:150px;"));
        block2->setFrameShape(QFrame::NoFrame);
        block2->setFrameShadow(QFrame::Plain);
        block2->setLineWidth(0);
        groupBox_3 = new QGroupBox(centralWidget);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setGeometry(QRect(534, 180, 132, 300));
        groupBox_3->setFocusPolicy(Qt::NoFocus);
        groupBox_3->setStyleSheet(QStringLiteral("border:0px;"));
        groupBox_3->setFlat(false);
        groupBox_3->setCheckable(false);
        groupBox_3->setChecked(false);
        day4 = new QLabel(groupBox_3);
        day4->setObjectName(QStringLiteral("day4"));
        day4->setGeometry(QRect(0, 0, 132, 50));
        day4->setFont(font6);
        day4->setFocusPolicy(Qt::NoFocus);
        day4->setAlignment(Qt::AlignCenter);
        imgday4 = new QLabel(groupBox_3);
        imgday4->setObjectName(QStringLiteral("imgday4"));
        imgday4->setGeometry(QRect(0, 60, 132, 141));
        imgday4->setFocusPolicy(Qt::NoFocus);
        imgday4->setStyleSheet(QString::fromUtf8("border-image: url(:/new/prefix1/image/\345\274\272\346\262\231\345\260\230\346\232\264.png);"));
        temperatrue4 = new QLabel(groupBox_3);
        temperatrue4->setObjectName(QStringLiteral("temperatrue4"));
        temperatrue4->setGeometry(QRect(0, 200, 132, 20));
        temperatrue4->setFont(font4);
        temperatrue4->setFocusPolicy(Qt::NoFocus);
        temperatrue4->setAlignment(Qt::AlignCenter);
        type4 = new QLabel(groupBox_3);
        type4->setObjectName(QStringLiteral("type4"));
        type4->setGeometry(QRect(0, 230, 132, 25));
        type4->setFont(font);
        type4->setFocusPolicy(Qt::NoFocus);
        type4->setAlignment(Qt::AlignCenter);
        feng4 = new QLabel(groupBox_3);
        feng4->setObjectName(QStringLiteral("feng4"));
        feng4->setGeometry(QRect(0, 253, 132, 30));
        feng4->setFont(font);
        feng4->setFocusPolicy(Qt::NoFocus);
        feng4->setAlignment(Qt::AlignCenter);
        block4 = new QLabel(groupBox_3);
        block4->setObjectName(QStringLiteral("block4"));
        block4->setGeometry(QRect(130, 0, 3, 301));
        block4->setStyleSheet(QLatin1String("background-color: rgb(237, 249, 255);\n"
"border-radius:150px;"));
        block4->setFrameShape(QFrame::NoFrame);
        block4->setFrameShadow(QFrame::Plain);
        block4->setLineWidth(0);
        day4->raise();
        temperatrue4->raise();
        type4->raise();
        feng4->raise();
        imgday4->raise();
        block4->raise();
        day3th = new QGroupBox(centralWidget);
        day3th->setObjectName(QStringLiteral("day3th"));
        day3th->setGeometry(QRect(402, 180, 132, 300));
        day3th->setFocusPolicy(Qt::NoFocus);
        day3th->setStyleSheet(QStringLiteral("border:0px;"));
        day3th->setFlat(false);
        day3th->setCheckable(false);
        day3th->setChecked(false);
        day3 = new QLabel(day3th);
        day3->setObjectName(QStringLiteral("day3"));
        day3->setGeometry(QRect(0, 0, 132, 50));
        day3->setFont(font6);
        day3->setFocusPolicy(Qt::NoFocus);
        day3->setAlignment(Qt::AlignCenter);
        imgday3 = new QLabel(day3th);
        imgday3->setObjectName(QStringLiteral("imgday3"));
        imgday3->setGeometry(QRect(0, 60, 132, 141));
        imgday3->setFocusPolicy(Qt::NoFocus);
        imgday3->setStyleSheet(QString::fromUtf8("border-image: url(:/new/prefix1/image/\346\232\264\351\233\250.png);"));
        temperature3 = new QLabel(day3th);
        temperature3->setObjectName(QStringLiteral("temperature3"));
        temperature3->setGeometry(QRect(0, 200, 132, 20));
        temperature3->setFont(font4);
        temperature3->setFocusPolicy(Qt::NoFocus);
        temperature3->setAlignment(Qt::AlignCenter);
        type3 = new QLabel(day3th);
        type3->setObjectName(QStringLiteral("type3"));
        type3->setGeometry(QRect(0, 230, 132, 25));
        type3->setFont(font);
        type3->setFocusPolicy(Qt::NoFocus);
        type3->setAlignment(Qt::AlignCenter);
        feng3 = new QLabel(day3th);
        feng3->setObjectName(QStringLiteral("feng3"));
        feng3->setGeometry(QRect(0, 253, 132, 30));
        feng3->setFont(font);
        feng3->setFocusPolicy(Qt::NoFocus);
        feng3->setAlignment(Qt::AlignCenter);
        block3 = new QLabel(day3th);
        block3->setObjectName(QStringLiteral("block3"));
        block3->setGeometry(QRect(130, 0, 3, 301));
        block3->setStyleSheet(QLatin1String("background-color: rgb(237, 249, 255);\n"
"border-radius:150px;"));
        block3->setFrameShape(QFrame::NoFrame);
        block3->setFrameShadow(QFrame::Plain);
        block3->setLineWidth(0);
        day5th = new QGroupBox(centralWidget);
        day5th->setObjectName(QStringLiteral("day5th"));
        day5th->setGeometry(QRect(666, 180, 132, 300));
        day5th->setFocusPolicy(Qt::NoFocus);
        day5th->setStyleSheet(QStringLiteral("border:0px;"));
        day5th->setFlat(false);
        day5th->setCheckable(false);
        day5th->setChecked(false);
        day5 = new QLabel(day5th);
        day5->setObjectName(QStringLiteral("day5"));
        day5->setGeometry(QRect(0, 0, 132, 50));
        day5->setFont(font6);
        day5->setFocusPolicy(Qt::NoFocus);
        day5->setAlignment(Qt::AlignCenter);
        imgday5 = new QLabel(day5th);
        imgday5->setObjectName(QStringLiteral("imgday5"));
        imgday5->setGeometry(QRect(0, 60, 132, 141));
        imgday5->setFocusPolicy(Qt::NoFocus);
        imgday5->setStyleSheet(QString::fromUtf8("border-image: url(:/new/prefix1/image/\351\230\265\351\233\250.png);"));
        temperature5 = new QLabel(day5th);
        temperature5->setObjectName(QStringLiteral("temperature5"));
        temperature5->setGeometry(QRect(0, 200, 132, 20));
        temperature5->setFont(font4);
        temperature5->setFocusPolicy(Qt::NoFocus);
        temperature5->setAlignment(Qt::AlignCenter);
        type5 = new QLabel(day5th);
        type5->setObjectName(QStringLiteral("type5"));
        type5->setGeometry(QRect(0, 230, 132, 25));
        type5->setFont(font);
        type5->setFocusPolicy(Qt::NoFocus);
        type5->setAlignment(Qt::AlignCenter);
        feng5 = new QLabel(day5th);
        feng5->setObjectName(QStringLiteral("feng5"));
        feng5->setGeometry(QRect(0, 253, 132, 30));
        feng5->setFont(font);
        feng5->setFocusPolicy(Qt::NoFocus);
        feng5->setAlignment(Qt::AlignCenter);
        gaomao = new QLabel(centralWidget);
        gaomao->setObjectName(QStringLiteral("gaomao"));
        gaomao->setGeometry(QRect(405, 40, 395, 91));
        gaomao->setFocusPolicy(Qt::NoFocus);
        gaomao->setFrameShape(QFrame::NoFrame);
        gaomao->setFrameShadow(QFrame::Plain);
        gaomao->setLineWidth(0);
        gaomao->setTextFormat(Qt::AutoText);
        gaomao->setScaledContents(false);
        gaomao->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        gaomao->setWordWrap(true);
        gaomao->setMargin(-35);
        gaomao->setIndent(39);
        gaomao->setOpenExternalLinks(false);
        labbar = new QLabel(centralWidget);
        labbar->setObjectName(QStringLiteral("labbar"));
        labbar->setGeometry(QRect(180, 0, 620, 30));
        labbar->setFocusPolicy(Qt::NoFocus);
        labbar->setLineWidth(0);
        softb = new softKey(centralWidget);
        softb->setObjectName(QStringLiteral("softb"));
        softb->setEnabled(true);
        softb->setGeometry(QRect(0, 179, 800, 300));
        rolllab = new rollLab(centralWidget);
        rolllab->setObjectName(QStringLiteral("rolllab"));
        rolllab->setGeometry(QRect(269, 130, 530, 50));
        closeBt = new QPushButton(centralWidget);
        closeBt->setObjectName(QStringLiteral("closeBt"));
        closeBt->setGeometry(QRect(770, 0, 30, 30));
        closeBt->setFocusPolicy(Qt::NoFocus);
        closeBt->setAutoFillBackground(false);
        closeBt->setStyleSheet(QString::fromUtf8("border-image: url(:/new/prefix1/image/\345\205\263\351\227\255\346\214\211\351\222\2561.png);\n"
"border:0px;"));
        closeBt->setFlat(true);
        hideBt = new QPushButton(centralWidget);
        hideBt->setObjectName(QStringLiteral("hideBt"));
        hideBt->setGeometry(QRect(740, 0, 30, 30));
        hideBt->setFocusPolicy(Qt::NoFocus);
        hideBt->setContextMenuPolicy(Qt::NoContextMenu);
        hideBt->setAutoFillBackground(false);
        hideBt->setStyleSheet(QString::fromUtf8("border-image: url(:/new/prefix1/image/\346\234\200\345\260\217\345\214\2261.png);\n"
"border:0px;"));
        hideBt->setCheckable(false);
        hideBt->setFlat(true);
        changPicBt = new QPushButton(centralWidget);
        changPicBt->setObjectName(QStringLiteral("changPicBt"));
        changPicBt->setGeometry(QRect(710, 0, 30, 30));
        QFont font7;
        font7.setKerning(true);
        changPicBt->setFont(font7);
        changPicBt->setFocusPolicy(Qt::NoFocus);
        changPicBt->setStyleSheet(QStringLiteral("border-image: url(:/new/prefix1/image/update.png);"));
        changPicBt->setFlat(true);
        weatherRE->setCentralWidget(centralWidget);

        retranslateUi(weatherRE);

        hideBt->setDefault(false);


        QMetaObject::connectSlotsByName(weatherRE);
    } // setupUi

    void retranslateUi(QMainWindow *weatherRE)
    {
        weatherRE->setWindowTitle(QApplication::translate("weatherRE", "weatherRE", Q_NULLPTR));
        pushButton->setText(QString());
        day1->setText(QApplication::translate("weatherRE", "02\346\234\21022\346\227\245 \345\221\250\344\272\224 \345\206\234\345\216\206\346\255\243\346\234\210\345\215\201\345\205\253", Q_NULLPTR));
        cityname->setText(QApplication::translate("weatherRE", "\345\271\277\345\267\236", Q_NULLPTR));
        day1st->setTitle(QString());
        today->setText(QApplication::translate("weatherRE", "\344\273\212\345\244\251", Q_NULLPTR));
        nowtmp->setText(QApplication::translate("weatherRE", "16\342\204\203", Q_NULLPTR));
        imgday1->setText(QString());
        temperature1->setText(QApplication::translate("weatherRE", "12 - 20\342\204\203", Q_NULLPTR));
        type1->setText(QApplication::translate("weatherRE", "\351\233\267\351\230\265\351\233\250", Q_NULLPTR));
        feng1->setText(QApplication::translate("weatherRE", "\344\270\234\351\243\216 3\347\272\247", Q_NULLPTR));
        block1->setText(QString());
        day2rd->setTitle(QString());
        day2->setText(QApplication::translate("weatherRE", "\345\221\250\345\205\255", Q_NULLPTR));
        imgday2->setText(QString());
        temperature2->setText(QApplication::translate("weatherRE", "12 - 20\342\204\203", Q_NULLPTR));
        type2->setText(QApplication::translate("weatherRE", "\351\233\267\351\230\265\351\233\250", Q_NULLPTR));
        feng2->setText(QApplication::translate("weatherRE", "\344\270\234\351\243\216 3\347\272\247", Q_NULLPTR));
        block2->setText(QString());
        groupBox_3->setTitle(QString());
        day4->setText(QApplication::translate("weatherRE", "\345\221\250\344\270\200", Q_NULLPTR));
        imgday4->setText(QString());
        temperatrue4->setText(QApplication::translate("weatherRE", "12 - 20\342\204\203", Q_NULLPTR));
        type4->setText(QApplication::translate("weatherRE", "\351\233\267\351\230\265\351\233\250", Q_NULLPTR));
        feng4->setText(QApplication::translate("weatherRE", "\344\270\234\351\243\216 3\347\272\247", Q_NULLPTR));
        block4->setText(QString());
        day3th->setTitle(QString());
        day3->setText(QApplication::translate("weatherRE", "\345\221\250\346\227\245", Q_NULLPTR));
        imgday3->setText(QString());
        temperature3->setText(QApplication::translate("weatherRE", "12 - 20\342\204\203", Q_NULLPTR));
        type3->setText(QApplication::translate("weatherRE", "\351\233\267\351\230\265\351\233\250", Q_NULLPTR));
        feng3->setText(QApplication::translate("weatherRE", "\344\270\234\351\243\216 3\347\272\247", Q_NULLPTR));
        block3->setText(QString());
        day5th->setTitle(QString());
        day5->setText(QApplication::translate("weatherRE", "\345\221\250\344\272\214", Q_NULLPTR));
        imgday5->setText(QString());
        temperature5->setText(QApplication::translate("weatherRE", "12 - 20\342\204\203", Q_NULLPTR));
        type5->setText(QApplication::translate("weatherRE", "\351\233\267\351\230\265\351\233\250", Q_NULLPTR));
        feng5->setText(QApplication::translate("weatherRE", "\344\270\234\351\243\216 3\347\272\247", Q_NULLPTR));
        gaomao->setText(QApplication::translate("weatherRE", "\346\230\274\345\244\234\346\270\251\345\267\256\350\276\203\345\244\247\357\274\214\350\276\203\346\230\223\345\217\221\347\224\237\346\204\237\345\206\222\357\274\214\350\257\267\351\200\202\345\275\223\345\242\236\345\207\217\350\241\243\346\234\215\343\200\202\344\275\223\350\264\250\350\276\203\345\274\261\347\232\204\346\234\213\345\217\213\350\257\267\346\263\250\346\204\217\351\230\262\346\212\244\343\200\202", Q_NULLPTR));
        labbar->setText(QString());
        closeBt->setText(QString());
        hideBt->setText(QString());
        changPicBt->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class weatherRE: public Ui_weatherRE {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WEATHERRE_H
