/********************************************************************************
** Form generated from reading UI file 'softkey.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SOFTKEY_H
#define UI_SOFTKEY_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>
#include <keybutton.h>

QT_BEGIN_NAMESPACE

class Ui_softKey
{
public:
    QGridLayout *gridLayout;
    QLineEdit *spellEdit;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *cn1;
    QPushButton *cn2;
    QPushButton *cn3;
    QPushButton *cn4;
    QPushButton *cn5;
    QPushButton *cn6;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *prevBt;
    QPushButton *nextBt;
    QHBoxLayout *horizontalLayout;
    keyButton *pushButton;
    keyButton *pushButton_2;
    keyButton *pushButton_7;
    keyButton *pushButton_3;
    keyButton *pushButton_8;
    keyButton *pushButton_4;
    keyButton *pushButton_9;
    keyButton *pushButton_5;
    keyButton *pushButton_10;
    keyButton *pushButton_6;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_2;
    keyButton *pushButton_11;
    keyButton *pushButton_18;
    keyButton *pushButton_12;
    keyButton *pushButton_19;
    keyButton *pushButton_13;
    keyButton *pushButton_14;
    keyButton *pushButton_15;
    keyButton *pushButton_16;
    keyButton *pushButton_17;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_3;
    keyButton *pushButton_20;
    keyButton *pushButton_24;
    keyButton *pushButton_21;
    keyButton *pushButton_25;
    keyButton *pushButton_22;
    keyButton *pushButton_26;
    keyButton *pushButton_23;
    QSpacerItem *horizontalSpacer_4;
    keyButton *pushButton_27;

    void setupUi(QWidget *softKey)
    {
        if (softKey->objectName().isEmpty())
            softKey->setObjectName(QStringLiteral("softKey"));
        softKey->resize(807, 300);
        softKey->setStyleSheet(QLatin1String("\n"
"QPushButton:hover\n"
"{	\n"
"	background-color: rgb(0, 170, 255);\n"
"}\n"
"QPushButton:press\n"
"{\n"
"	\n"
"	background-color: rgb(0, 85, 255);\n"
"}"));
        gridLayout = new QGridLayout(softKey);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        spellEdit = new QLineEdit(softKey);
        spellEdit->setObjectName(QStringLiteral("spellEdit"));
        spellEdit->setMouseTracking(true);
        spellEdit->setFrame(false);

        gridLayout->addWidget(spellEdit, 0, 0, 1, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        cn1 = new QPushButton(softKey);
        cn1->setObjectName(QStringLiteral("cn1"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(cn1->sizePolicy().hasHeightForWidth());
        cn1->setSizePolicy(sizePolicy);
        QFont font;
        font.setFamily(QString::fromUtf8("Adobe \346\245\267\344\275\223 Std R"));
        font.setPointSize(20);
        cn1->setFont(font);
        cn1->setFocusPolicy(Qt::NoFocus);
        cn1->setFlat(true);

        horizontalLayout_4->addWidget(cn1);

        cn2 = new QPushButton(softKey);
        cn2->setObjectName(QStringLiteral("cn2"));
        sizePolicy.setHeightForWidth(cn2->sizePolicy().hasHeightForWidth());
        cn2->setSizePolicy(sizePolicy);
        cn2->setFont(font);
        cn2->setFocusPolicy(Qt::NoFocus);
        cn2->setFlat(true);

        horizontalLayout_4->addWidget(cn2);

        cn3 = new QPushButton(softKey);
        cn3->setObjectName(QStringLiteral("cn3"));
        sizePolicy.setHeightForWidth(cn3->sizePolicy().hasHeightForWidth());
        cn3->setSizePolicy(sizePolicy);
        cn3->setFont(font);
        cn3->setFocusPolicy(Qt::NoFocus);
        cn3->setFlat(true);

        horizontalLayout_4->addWidget(cn3);

        cn4 = new QPushButton(softKey);
        cn4->setObjectName(QStringLiteral("cn4"));
        sizePolicy.setHeightForWidth(cn4->sizePolicy().hasHeightForWidth());
        cn4->setSizePolicy(sizePolicy);
        cn4->setFont(font);
        cn4->setFocusPolicy(Qt::NoFocus);
        cn4->setFlat(true);

        horizontalLayout_4->addWidget(cn4);

        cn5 = new QPushButton(softKey);
        cn5->setObjectName(QStringLiteral("cn5"));
        sizePolicy.setHeightForWidth(cn5->sizePolicy().hasHeightForWidth());
        cn5->setSizePolicy(sizePolicy);
        cn5->setFont(font);
        cn5->setFocusPolicy(Qt::NoFocus);
        cn5->setFlat(true);

        horizontalLayout_4->addWidget(cn5);

        cn6 = new QPushButton(softKey);
        cn6->setObjectName(QStringLiteral("cn6"));
        sizePolicy.setHeightForWidth(cn6->sizePolicy().hasHeightForWidth());
        cn6->setSizePolicy(sizePolicy);
        cn6->setFont(font);
        cn6->setFocusPolicy(Qt::NoFocus);
        cn6->setFlat(true);

        horizontalLayout_4->addWidget(cn6);

        horizontalSpacer_5 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_5);

        prevBt = new QPushButton(softKey);
        prevBt->setObjectName(QStringLiteral("prevBt"));
        sizePolicy.setHeightForWidth(prevBt->sizePolicy().hasHeightForWidth());
        prevBt->setSizePolicy(sizePolicy);
        prevBt->setMaximumSize(QSize(40, 16777215));
        QFont font1;
        font1.setPointSize(20);
        prevBt->setFont(font1);
        prevBt->setFocusPolicy(Qt::NoFocus);
        prevBt->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout_4->addWidget(prevBt);

        nextBt = new QPushButton(softKey);
        nextBt->setObjectName(QStringLiteral("nextBt"));
        sizePolicy.setHeightForWidth(nextBt->sizePolicy().hasHeightForWidth());
        nextBt->setSizePolicy(sizePolicy);
        nextBt->setMaximumSize(QSize(40, 16777215));
        nextBt->setFont(font1);
        nextBt->setFocusPolicy(Qt::NoFocus);
        nextBt->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout_4->addWidget(nextBt);


        gridLayout->addLayout(horizontalLayout_4, 1, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        pushButton = new keyButton(softKey);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        sizePolicy.setHeightForWidth(pushButton->sizePolicy().hasHeightForWidth());
        pushButton->setSizePolicy(sizePolicy);
        pushButton->setMinimumSize(QSize(72, 0));
        QFont font2;
        font2.setPointSize(15);
        pushButton->setFont(font2);
        pushButton->setFocusPolicy(Qt::NoFocus);
        pushButton->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout->addWidget(pushButton);

        pushButton_2 = new keyButton(softKey);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        sizePolicy.setHeightForWidth(pushButton_2->sizePolicy().hasHeightForWidth());
        pushButton_2->setSizePolicy(sizePolicy);
        pushButton_2->setMinimumSize(QSize(72, 0));
        pushButton_2->setFont(font2);
        pushButton_2->setFocusPolicy(Qt::NoFocus);
        pushButton_2->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout->addWidget(pushButton_2);

        pushButton_7 = new keyButton(softKey);
        pushButton_7->setObjectName(QStringLiteral("pushButton_7"));
        sizePolicy.setHeightForWidth(pushButton_7->sizePolicy().hasHeightForWidth());
        pushButton_7->setSizePolicy(sizePolicy);
        pushButton_7->setMinimumSize(QSize(72, 0));
        pushButton_7->setFont(font2);
        pushButton_7->setFocusPolicy(Qt::NoFocus);
        pushButton_7->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout->addWidget(pushButton_7);

        pushButton_3 = new keyButton(softKey);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        sizePolicy.setHeightForWidth(pushButton_3->sizePolicy().hasHeightForWidth());
        pushButton_3->setSizePolicy(sizePolicy);
        pushButton_3->setMinimumSize(QSize(72, 0));
        pushButton_3->setFont(font2);
        pushButton_3->setFocusPolicy(Qt::NoFocus);
        pushButton_3->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout->addWidget(pushButton_3);

        pushButton_8 = new keyButton(softKey);
        pushButton_8->setObjectName(QStringLiteral("pushButton_8"));
        sizePolicy.setHeightForWidth(pushButton_8->sizePolicy().hasHeightForWidth());
        pushButton_8->setSizePolicy(sizePolicy);
        pushButton_8->setMinimumSize(QSize(72, 0));
        pushButton_8->setFont(font2);
        pushButton_8->setFocusPolicy(Qt::NoFocus);
        pushButton_8->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout->addWidget(pushButton_8);

        pushButton_4 = new keyButton(softKey);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));
        sizePolicy.setHeightForWidth(pushButton_4->sizePolicy().hasHeightForWidth());
        pushButton_4->setSizePolicy(sizePolicy);
        pushButton_4->setMinimumSize(QSize(72, 0));
        pushButton_4->setFont(font2);
        pushButton_4->setFocusPolicy(Qt::NoFocus);
        pushButton_4->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout->addWidget(pushButton_4);

        pushButton_9 = new keyButton(softKey);
        pushButton_9->setObjectName(QStringLiteral("pushButton_9"));
        sizePolicy.setHeightForWidth(pushButton_9->sizePolicy().hasHeightForWidth());
        pushButton_9->setSizePolicy(sizePolicy);
        pushButton_9->setMinimumSize(QSize(72, 0));
        pushButton_9->setFont(font2);
        pushButton_9->setFocusPolicy(Qt::NoFocus);
        pushButton_9->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout->addWidget(pushButton_9);

        pushButton_5 = new keyButton(softKey);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));
        sizePolicy.setHeightForWidth(pushButton_5->sizePolicy().hasHeightForWidth());
        pushButton_5->setSizePolicy(sizePolicy);
        pushButton_5->setMinimumSize(QSize(72, 0));
        pushButton_5->setFont(font2);
        pushButton_5->setFocusPolicy(Qt::NoFocus);
        pushButton_5->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout->addWidget(pushButton_5);

        pushButton_10 = new keyButton(softKey);
        pushButton_10->setObjectName(QStringLiteral("pushButton_10"));
        sizePolicy.setHeightForWidth(pushButton_10->sizePolicy().hasHeightForWidth());
        pushButton_10->setSizePolicy(sizePolicy);
        pushButton_10->setMinimumSize(QSize(72, 0));
        pushButton_10->setFont(font2);
        pushButton_10->setFocusPolicy(Qt::NoFocus);
        pushButton_10->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout->addWidget(pushButton_10);

        pushButton_6 = new keyButton(softKey);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));
        sizePolicy.setHeightForWidth(pushButton_6->sizePolicy().hasHeightForWidth());
        pushButton_6->setSizePolicy(sizePolicy);
        pushButton_6->setMinimumSize(QSize(72, 0));
        pushButton_6->setFont(font2);
        pushButton_6->setFocusPolicy(Qt::NoFocus);
        pushButton_6->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout->addWidget(pushButton_6);


        gridLayout->addLayout(horizontalLayout, 2, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        pushButton_11 = new keyButton(softKey);
        pushButton_11->setObjectName(QStringLiteral("pushButton_11"));
        sizePolicy.setHeightForWidth(pushButton_11->sizePolicy().hasHeightForWidth());
        pushButton_11->setSizePolicy(sizePolicy);
        pushButton_11->setMinimumSize(QSize(72, 0));
        pushButton_11->setMaximumSize(QSize(72, 16777215));
        pushButton_11->setFont(font2);
        pushButton_11->setFocusPolicy(Qt::NoFocus);
        pushButton_11->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout_2->addWidget(pushButton_11);

        pushButton_18 = new keyButton(softKey);
        pushButton_18->setObjectName(QStringLiteral("pushButton_18"));
        sizePolicy.setHeightForWidth(pushButton_18->sizePolicy().hasHeightForWidth());
        pushButton_18->setSizePolicy(sizePolicy);
        pushButton_18->setMinimumSize(QSize(72, 0));
        pushButton_18->setMaximumSize(QSize(72, 16777215));
        pushButton_18->setFont(font2);
        pushButton_18->setFocusPolicy(Qt::NoFocus);
        pushButton_18->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout_2->addWidget(pushButton_18);

        pushButton_12 = new keyButton(softKey);
        pushButton_12->setObjectName(QStringLiteral("pushButton_12"));
        sizePolicy.setHeightForWidth(pushButton_12->sizePolicy().hasHeightForWidth());
        pushButton_12->setSizePolicy(sizePolicy);
        pushButton_12->setMinimumSize(QSize(72, 0));
        pushButton_12->setMaximumSize(QSize(72, 16777215));
        pushButton_12->setFont(font2);
        pushButton_12->setFocusPolicy(Qt::NoFocus);
        pushButton_12->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout_2->addWidget(pushButton_12);

        pushButton_19 = new keyButton(softKey);
        pushButton_19->setObjectName(QStringLiteral("pushButton_19"));
        sizePolicy.setHeightForWidth(pushButton_19->sizePolicy().hasHeightForWidth());
        pushButton_19->setSizePolicy(sizePolicy);
        pushButton_19->setMinimumSize(QSize(72, 0));
        pushButton_19->setMaximumSize(QSize(72, 16777215));
        pushButton_19->setFont(font2);
        pushButton_19->setFocusPolicy(Qt::NoFocus);
        pushButton_19->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout_2->addWidget(pushButton_19);

        pushButton_13 = new keyButton(softKey);
        pushButton_13->setObjectName(QStringLiteral("pushButton_13"));
        sizePolicy.setHeightForWidth(pushButton_13->sizePolicy().hasHeightForWidth());
        pushButton_13->setSizePolicy(sizePolicy);
        pushButton_13->setMinimumSize(QSize(72, 0));
        pushButton_13->setMaximumSize(QSize(72, 16777215));
        pushButton_13->setFont(font2);
        pushButton_13->setFocusPolicy(Qt::NoFocus);
        pushButton_13->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout_2->addWidget(pushButton_13);

        pushButton_14 = new keyButton(softKey);
        pushButton_14->setObjectName(QStringLiteral("pushButton_14"));
        sizePolicy.setHeightForWidth(pushButton_14->sizePolicy().hasHeightForWidth());
        pushButton_14->setSizePolicy(sizePolicy);
        pushButton_14->setMinimumSize(QSize(72, 0));
        pushButton_14->setMaximumSize(QSize(72, 16777215));
        pushButton_14->setFont(font2);
        pushButton_14->setFocusPolicy(Qt::NoFocus);
        pushButton_14->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout_2->addWidget(pushButton_14);

        pushButton_15 = new keyButton(softKey);
        pushButton_15->setObjectName(QStringLiteral("pushButton_15"));
        sizePolicy.setHeightForWidth(pushButton_15->sizePolicy().hasHeightForWidth());
        pushButton_15->setSizePolicy(sizePolicy);
        pushButton_15->setMinimumSize(QSize(72, 0));
        pushButton_15->setMaximumSize(QSize(72, 16777215));
        pushButton_15->setFont(font2);
        pushButton_15->setFocusPolicy(Qt::NoFocus);
        pushButton_15->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout_2->addWidget(pushButton_15);

        pushButton_16 = new keyButton(softKey);
        pushButton_16->setObjectName(QStringLiteral("pushButton_16"));
        sizePolicy.setHeightForWidth(pushButton_16->sizePolicy().hasHeightForWidth());
        pushButton_16->setSizePolicy(sizePolicy);
        pushButton_16->setMinimumSize(QSize(72, 0));
        pushButton_16->setMaximumSize(QSize(72, 16777215));
        pushButton_16->setFont(font2);
        pushButton_16->setFocusPolicy(Qt::NoFocus);
        pushButton_16->setStyleSheet(QLatin1String("border:1px solid#ffffff;\n"
"border-radius:15px;"));

        horizontalLayout_2->addWidget(pushButton_16);

        pushButton_17 = new keyButton(softKey);
        pushButton_17->setObjectName(QStringLiteral("pushButton_17"));
        sizePolicy.setHeightForWidth(pushButton_17->sizePolicy().hasHeightForWidth());
        pushButton_17->setSizePolicy(sizePolicy);
        pushButton_17->setMinimumSize(QSize(72, 0));
        pushButton_17->setMaximumSize(QSize(72, 16777215));
        pushButton_17->setFont(font2);
        pushButton_17->setFocusPolicy(Qt::NoFocus);
        pushButton_17->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout_2->addWidget(pushButton_17);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);


        gridLayout->addLayout(horizontalLayout_2, 3, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        pushButton_20 = new keyButton(softKey);
        pushButton_20->setObjectName(QStringLiteral("pushButton_20"));
        sizePolicy.setHeightForWidth(pushButton_20->sizePolicy().hasHeightForWidth());
        pushButton_20->setSizePolicy(sizePolicy);
        pushButton_20->setMinimumSize(QSize(72, 0));
        pushButton_20->setMaximumSize(QSize(72, 16777215));
        pushButton_20->setFont(font2);
        pushButton_20->setFocusPolicy(Qt::NoFocus);
        pushButton_20->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout_3->addWidget(pushButton_20);

        pushButton_24 = new keyButton(softKey);
        pushButton_24->setObjectName(QStringLiteral("pushButton_24"));
        sizePolicy.setHeightForWidth(pushButton_24->sizePolicy().hasHeightForWidth());
        pushButton_24->setSizePolicy(sizePolicy);
        pushButton_24->setMinimumSize(QSize(72, 0));
        pushButton_24->setMaximumSize(QSize(72, 16777215));
        pushButton_24->setFont(font2);
        pushButton_24->setFocusPolicy(Qt::NoFocus);
        pushButton_24->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout_3->addWidget(pushButton_24);

        pushButton_21 = new keyButton(softKey);
        pushButton_21->setObjectName(QStringLiteral("pushButton_21"));
        sizePolicy.setHeightForWidth(pushButton_21->sizePolicy().hasHeightForWidth());
        pushButton_21->setSizePolicy(sizePolicy);
        pushButton_21->setMinimumSize(QSize(72, 0));
        pushButton_21->setMaximumSize(QSize(72, 16777215));
        pushButton_21->setFont(font2);
        pushButton_21->setFocusPolicy(Qt::NoFocus);
        pushButton_21->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout_3->addWidget(pushButton_21);

        pushButton_25 = new keyButton(softKey);
        pushButton_25->setObjectName(QStringLiteral("pushButton_25"));
        sizePolicy.setHeightForWidth(pushButton_25->sizePolicy().hasHeightForWidth());
        pushButton_25->setSizePolicy(sizePolicy);
        pushButton_25->setMinimumSize(QSize(72, 0));
        pushButton_25->setMaximumSize(QSize(72, 16777215));
        pushButton_25->setFont(font2);
        pushButton_25->setFocusPolicy(Qt::NoFocus);
        pushButton_25->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout_3->addWidget(pushButton_25);

        pushButton_22 = new keyButton(softKey);
        pushButton_22->setObjectName(QStringLiteral("pushButton_22"));
        sizePolicy.setHeightForWidth(pushButton_22->sizePolicy().hasHeightForWidth());
        pushButton_22->setSizePolicy(sizePolicy);
        pushButton_22->setMinimumSize(QSize(72, 0));
        pushButton_22->setMaximumSize(QSize(72, 16777215));
        pushButton_22->setFont(font2);
        pushButton_22->setFocusPolicy(Qt::NoFocus);
        pushButton_22->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout_3->addWidget(pushButton_22);

        pushButton_26 = new keyButton(softKey);
        pushButton_26->setObjectName(QStringLiteral("pushButton_26"));
        sizePolicy.setHeightForWidth(pushButton_26->sizePolicy().hasHeightForWidth());
        pushButton_26->setSizePolicy(sizePolicy);
        pushButton_26->setMinimumSize(QSize(72, 0));
        pushButton_26->setMaximumSize(QSize(72, 16777215));
        pushButton_26->setFont(font2);
        pushButton_26->setFocusPolicy(Qt::NoFocus);
        pushButton_26->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout_3->addWidget(pushButton_26);

        pushButton_23 = new keyButton(softKey);
        pushButton_23->setObjectName(QStringLiteral("pushButton_23"));
        sizePolicy.setHeightForWidth(pushButton_23->sizePolicy().hasHeightForWidth());
        pushButton_23->setSizePolicy(sizePolicy);
        pushButton_23->setMinimumSize(QSize(72, 0));
        pushButton_23->setMaximumSize(QSize(72, 16777215));
        pushButton_23->setFont(font2);
        pushButton_23->setFocusPolicy(Qt::NoFocus);
        pushButton_23->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout_3->addWidget(pushButton_23);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_4);

        pushButton_27 = new keyButton(softKey);
        pushButton_27->setObjectName(QStringLiteral("pushButton_27"));
        sizePolicy.setHeightForWidth(pushButton_27->sizePolicy().hasHeightForWidth());
        pushButton_27->setSizePolicy(sizePolicy);
        pushButton_27->setMinimumSize(QSize(72, 0));
        pushButton_27->setMaximumSize(QSize(72, 16777215));
        pushButton_27->setFont(font2);
        pushButton_27->setFocusPolicy(Qt::NoFocus);
        pushButton_27->setStyleSheet(QLatin1String("	border:1px solid#ffffff;\n"
"	border-radius:15px;"));

        horizontalLayout_3->addWidget(pushButton_27);


        gridLayout->addLayout(horizontalLayout_3, 4, 0, 1, 1);


        retranslateUi(softKey);

        QMetaObject::connectSlotsByName(softKey);
    } // setupUi

    void retranslateUi(QWidget *softKey)
    {
        softKey->setWindowTitle(QApplication::translate("softKey", "softKey", Q_NULLPTR));
        spellEdit->setText(QString());
        cn1->setText(QString());
        cn2->setText(QString());
        cn3->setText(QString());
        cn4->setText(QString());
        cn5->setText(QString());
        cn6->setText(QString());
        prevBt->setText(QApplication::translate("softKey", "\342\206\221", Q_NULLPTR));
        nextBt->setText(QApplication::translate("softKey", "\342\206\223", Q_NULLPTR));
        pushButton->setText(QApplication::translate("softKey", "q", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("softKey", "w", Q_NULLPTR));
        pushButton_7->setText(QApplication::translate("softKey", "e", Q_NULLPTR));
        pushButton_3->setText(QApplication::translate("softKey", "r", Q_NULLPTR));
        pushButton_8->setText(QApplication::translate("softKey", "t", Q_NULLPTR));
        pushButton_4->setText(QApplication::translate("softKey", "y", Q_NULLPTR));
        pushButton_9->setText(QApplication::translate("softKey", "u", Q_NULLPTR));
        pushButton_5->setText(QApplication::translate("softKey", "i", Q_NULLPTR));
        pushButton_10->setText(QApplication::translate("softKey", "o", Q_NULLPTR));
        pushButton_6->setText(QApplication::translate("softKey", "p", Q_NULLPTR));
        pushButton_11->setText(QApplication::translate("softKey", "a", Q_NULLPTR));
        pushButton_18->setText(QApplication::translate("softKey", "s", Q_NULLPTR));
        pushButton_12->setText(QApplication::translate("softKey", "d", Q_NULLPTR));
        pushButton_19->setText(QApplication::translate("softKey", "f", Q_NULLPTR));
        pushButton_13->setText(QApplication::translate("softKey", "g", Q_NULLPTR));
        pushButton_14->setText(QApplication::translate("softKey", "h", Q_NULLPTR));
        pushButton_15->setText(QApplication::translate("softKey", "j", Q_NULLPTR));
        pushButton_16->setText(QApplication::translate("softKey", "k", Q_NULLPTR));
        pushButton_17->setText(QApplication::translate("softKey", "l", Q_NULLPTR));
        pushButton_20->setText(QApplication::translate("softKey", "z", Q_NULLPTR));
        pushButton_24->setText(QApplication::translate("softKey", "x", Q_NULLPTR));
        pushButton_21->setText(QApplication::translate("softKey", "c", Q_NULLPTR));
        pushButton_25->setText(QApplication::translate("softKey", "v", Q_NULLPTR));
        pushButton_22->setText(QApplication::translate("softKey", "b", Q_NULLPTR));
        pushButton_26->setText(QApplication::translate("softKey", "n", Q_NULLPTR));
        pushButton_23->setText(QApplication::translate("softKey", "m", Q_NULLPTR));
        pushButton_27->setText(QApplication::translate("softKey", "\342\206\220", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class softKey: public Ui_softKey {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SOFTKEY_H
