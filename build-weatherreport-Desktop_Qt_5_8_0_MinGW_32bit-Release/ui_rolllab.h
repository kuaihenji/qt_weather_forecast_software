/********************************************************************************
** Form generated from reading UI file 'rolllab.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ROLLLAB_H
#define UI_ROLLLAB_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_rollLab
{
public:

    void setupUi(QWidget *rollLab)
    {
        if (rollLab->objectName().isEmpty())
            rollLab->setObjectName(QStringLiteral("rollLab"));
        rollLab->resize(530, 50);

        retranslateUi(rollLab);

        QMetaObject::connectSlotsByName(rollLab);
    } // setupUi

    void retranslateUi(QWidget *rollLab)
    {
        rollLab->setWindowTitle(QApplication::translate("rollLab", "rollLab", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class rollLab: public Ui_rollLab {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ROLLLAB_H
