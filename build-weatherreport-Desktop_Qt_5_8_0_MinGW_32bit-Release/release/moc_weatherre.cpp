/****************************************************************************
** Meta object code from reading C++ file 'weatherre.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../weatherreport/weatherre.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'weatherre.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_weatherRE_t {
    QByteArrayData data[13];
    char stringdata0[170];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_weatherRE_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_weatherRE_t qt_meta_stringdata_weatherRE = {
    {
QT_MOC_LITERAL(0, 0, 9), // "weatherRE"
QT_MOC_LITERAL(1, 10, 23), // "on_lineEdit_textChanged"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 4), // "arg1"
QT_MOC_LITERAL(4, 40, 12), // "line_setText"
QT_MOC_LITERAL(5, 53, 3), // "str"
QT_MOC_LITERAL(6, 57, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(7, 79, 9), // "read_json"
QT_MOC_LITERAL(8, 89, 14), // "QNetworkReply*"
QT_MOC_LITERAL(9, 104, 6), // "replay"
QT_MOC_LITERAL(10, 111, 18), // "on_closeBt_clicked"
QT_MOC_LITERAL(11, 130, 17), // "on_hideBt_clicked"
QT_MOC_LITERAL(12, 148, 21) // "on_changPicBt_clicked"

    },
    "weatherRE\0on_lineEdit_textChanged\0\0"
    "arg1\0line_setText\0str\0on_pushButton_clicked\0"
    "read_json\0QNetworkReply*\0replay\0"
    "on_closeBt_clicked\0on_hideBt_clicked\0"
    "on_changPicBt_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_weatherRE[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   49,    2, 0x08 /* Private */,
       4,    1,   52,    2, 0x08 /* Private */,
       6,    0,   55,    2, 0x08 /* Private */,
       7,    1,   56,    2, 0x08 /* Private */,
      10,    0,   59,    2, 0x08 /* Private */,
      11,    0,   60,    2, 0x08 /* Private */,
      12,    0,   61,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void weatherRE::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        weatherRE *_t = static_cast<weatherRE *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_lineEdit_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->line_setText((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->on_pushButton_clicked(); break;
        case 3: _t->read_json((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 4: _t->on_closeBt_clicked(); break;
        case 5: _t->on_hideBt_clicked(); break;
        case 6: _t->on_changPicBt_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QNetworkReply* >(); break;
            }
            break;
        }
    }
}

const QMetaObject weatherRE::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_weatherRE.data,
      qt_meta_data_weatherRE,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *weatherRE::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *weatherRE::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_weatherRE.stringdata0))
        return static_cast<void*>(const_cast< weatherRE*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int weatherRE::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
