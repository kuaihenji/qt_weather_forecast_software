#ifndef SOFTKEY_H
#define SOFTKEY_H

#include <QWidget>
#include<QPushButton>
#include<QMap>
#include<QString>
#include<QFile>
#include<QPainter>
#include<QPaintEvent>
namespace Ui {
class softKey;
}

class softKey : public  QWidget
{
    Q_OBJECT

public:
    explicit softKey(QWidget *parent = 0);
    ~softKey();
    void reset_btStyle();
    void paintEvent(QPaintEvent *event);
signals:
    void send_cn(QString);
private slots:
    void on_prevBt_clicked();

    void on_nextBt_clicked();
    void which_cn();

    void on_spellEdit_textChanged(const QString &arg1);

private:
    Ui::softKey *ui;
    QPushButton *cnBT[6];


    //strList[ai]=埃挨哎唉哀皑癌蔼矮艾碍爱隘捱嗳嗌嫒瑷暧砹锿霭
    QMap<QString,QString> strList;
    int postion;

   //埃挨哎唉哀皑癌蔼矮艾碍爱隘捱嗳嗌嫒瑷暧砹锿霭
    QStringList cnList;
};

#endif // SOFTKEY_H
