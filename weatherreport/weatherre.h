#ifndef WEATHERRE_H
#define WEATHERRE_H

#include <QMainWindow>
#include<QKeyEvent>
#include<QApplication>
#include<QDebug>
#include<QEvent>
#include<QMap>
#include<QFile>
#include<QLabel>
#include<QNetworkAccessManager>
#include<QNetworkReply>
#include<QNetworkRequest>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonDocument>
#include <QJsonParseError>
#include<QPainter>
#include<QPaintEvent>
#include<QDateTime>
namespace Ui {
class weatherRE;
}

class weatherRE : public QMainWindow
{
    Q_OBJECT

public:
    explicit weatherRE(QWidget *parent = 0);
    ~weatherRE();
    bool eventFilter(QObject *watched, QEvent *event);
    void setEvetFilter();
    void paresingJson(QString json);
    void lable_init();
    void chang_weatherImg();
    void darw_block();

private slots:
    void on_lineEdit_textChanged(const QString &arg1);
    void line_setText(QString str);
    void on_pushButton_clicked();
    void read_json(QNetworkReply*replay);

    void on_closeBt_clicked();

    void on_hideBt_clicked();

    void on_changPicBt_clicked();

private:
    Ui::weatherRE *ui;
    QMap<QString,QString> cityKeys;
    int newlength;
    int lastlength;
    int selectBG;
    QNetworkAccessManager manager;
    QLabel *feng[5],*temperature[5],*date[5],
                    *img[5],*forcasttype[5];
};

#endif // WEATHERRE_H
