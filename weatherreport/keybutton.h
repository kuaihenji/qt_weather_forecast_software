#ifndef KEYBUTTON_H
#define KEYBUTTON_H

#include <QWidget>
#include<QPushButton>
#include<QObject>
#include<QKeyEvent>
#include<QApplication>
namespace Ui {
class keyButton;
}

class keyButton : public QPushButton
{
    Q_OBJECT

public:
    explicit keyButton(QWidget *parent = Q_NULLPTR);
    ~keyButton();

private:
    Ui::keyButton *ui;
private slots:
    void button_onClicked();
};

#endif // KEYBUTTON_H
