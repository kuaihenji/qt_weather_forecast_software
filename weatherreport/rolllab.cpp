#include "rolllab.h"
#include "ui_rolllab.h"

rollLab::rollLab(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::rollLab)
{
    ui->setupUi(this);
    this->x=this->width();
    QObject::connect(&rolltimer,SIGNAL(timeout()),this,SLOT(str_move()));
}

rollLab::~rollLab()
{
    delete ui;
}

void rollLab::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    QFontMetrics met(this->font());
    int tw = met.width(this->rollstr);
    int th = met.height();
    //绘制文字
    if(x<0 && x+tw>=0)
    {
        painter.drawText(x, th, rollstr);
        painter.drawText(this->width()+x, th, rollstr);
        if(x+tw == 0)
        {
            x = this->width()-tw;
        }

    }else
    {
        painter.drawText(x, th, rollstr);
    }

}

void rollLab::set_rollstr(const QString &str)
{
    this->rolltimer.stop();
    this->rollstr=str;
    this->rolltimer.start(25);
}

void rollLab::str_move()
{

    x-=1;
    update();
}
