#include "keybutton.h"
#include <QApplication>
#include<QDebug>
keyButton::keyButton(QWidget *parent) :QPushButton(parent)
{

    QObject::connect(this,SIGNAL(clicked(bool)),this,SLOT(button_onClicked()));
}

keyButton::~keyButton()
{
    delete ui;
}
void keyButton::button_onClicked()
{
    QString key_str=this->text();
    QKeyEvent *key=NULL;
    if(key_str=="←")
    {

        key=new QKeyEvent(QKeyEvent::KeyPress,Qt::Key_Backspace,
                                                    Qt::NoModifier,key_str);
    }
    else
    {
        char ascii_key= key_str.at(0).toLatin1();
        key=new QKeyEvent(QKeyEvent::KeyPress,ascii_key,
                                                                            Qt::NoModifier,key_str);
    }
    QApplication::postEvent(QApplication::focusWidget(),key);
}
