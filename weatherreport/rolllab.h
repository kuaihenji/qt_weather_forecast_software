#ifndef ROLLLAB_H
#define ROLLLAB_H

#include <QWidget>
#include<QPaintEvent>
#include<QTimer>
#include<QPainter>
namespace Ui {
class rollLab;
}

class rollLab : public QWidget
{
    Q_OBJECT

public:
    explicit rollLab(QWidget *parent = 0);
    ~rollLab();
    void paintEvent(QPaintEvent *event);
    void set_rollstr(const QString &str);
private slots:
    void str_move();
private:
    Ui::rollLab *ui;
    QTimer rolltimer;
    int x;
    QString rollstr;
};

#endif // ROLLLAB_H
